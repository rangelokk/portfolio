using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class AttackSis : MonoBehaviour
{

    public List<Battle> battles;
    

    public int numberOfBattle;
    public int numberOfAttack;

    
 
    void Start()
    {
        
    }

    public void StartBattle()
    {

        StartCoroutine(nextAtt());


    }

    IEnumerator nextAtt() {
        Debug.Log("Start");
        while (numberOfAttack != battles[numberOfBattle].battle.Count)
        {
            GameObject attack = Instantiate(battles[numberOfBattle].battle[numberOfAttack].attack.form, battles[numberOfBattle].battle[numberOfAttack].transform, battles[numberOfBattle].battle[numberOfAttack].attack.form.transform.rotation);
           

            yield return new WaitForSeconds(battles[numberOfBattle].battle[numberOfAttack].time);
            numberOfAttack++;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    
}
[Serializable]
public class AttackPattern
{
    public GameObject form;
    public float speed;
    public Vector2 vector;

    
}
[Serializable]
public class AttOptions
{
    public AttackPattern attack;
    public Vector2 transform;
    public float time;
}
[Serializable]
public class Battle
{
    public List<AttOptions> battle;
}