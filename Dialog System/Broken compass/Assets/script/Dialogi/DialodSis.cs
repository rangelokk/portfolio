﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Playables;

public class DialodSis : MonoBehaviour

{
    public PlayableDirector director; ///удалить
    
  
    public Image chaim;
    public Transform panel;
    public Button btnPrefab;

    public List<pers> pers;
    public Text wind;
    public Text name;
    public GameObject dia;

    public int number_frase = 0;
    private int scene_number; //номер диалога
    [HideInInspector]
    public bool active = false;
    
    public List<Dialog> memoris;
    public List<Scenes> scenes;

    private bool CorutinWork = false;
    private bool waiting;

    private Dialog tecFrase;
   

    // Start is called before the first frame update
    void Start()
    {
        dia.SetActive(false);
        director = GetComponent<PlayableDirector>();
    }
    public void SledDialog()
    {
        bool correct = true;
        if (number_frase != scenes[scene_number].Frases.Count - 1)
        {
            if (!CorutinWork && !waiting && active)
            {
                if (scenes[scene_number].Frases[number_frase + 1].need.Count != 0)
                    foreach (Reaction reaction in scenes[scene_number].Frases[number_frase + 1].need)
                    {
                        if (reaction.poits.x <= pers[(int)reaction.charapter].sympaty && pers[(int)reaction.charapter].sympaty < reaction.poits.y)
                            continue;
                        else
                        {
                            number_frase++;
                            tecFrase = scenes[scene_number].Frases[number_frase];
                            SledDialog();
                            correct = false;
                            Debug.Log("Next");
                            break;
                        }
                    }
                if (correct)
                {
                    number_frase++;
                    if (number_frase != scenes[scene_number].Frases.Count)
                    {
                        tecFrase = scenes[scene_number].Frases[number_frase];
                        Say();
                    }
                }
            }
        }
        else DialogOff();
    }



    public void SaveMemory()
    {
        if (!tecFrase.saved)
        {
            memoris.Add(tecFrase);
            tecFrase.saved = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
       
    }
    public void DialogOff()
    {
        number_frase = 0;
        dia.SetActive(false);
        active = false;

        if (scene_number == 0) director.Play(); ///
    }

    private void Say()
    {
        Dialog.Charapter chara = tecFrase.charapter;
        if (chara == Dialog.Charapter.Вопрос)
        {
            pers[0].sympaty = 0;
            for (int i = 0; i < tecFrase.Variants.Count; i++)
            {
                CreateButton(tecFrase.Variants[i], i);
            }
            waiting = true;
        }
        else
        {
            Dialog.Emotion feel = tecFrase.emotion;
            string eto = tecFrase.text;

            if (tecFrase.reaction.Count != 0)
            {
                foreach (Reaction point in tecFrase.reaction)
                {
                    pers[(int)point.charapter].sympaty += point.poits.x;
                }
            }
            if(pers[(int)chara].emotion[(int)feel] != null)
            chaim.sprite = pers[(int)chara].emotion[(int)feel];
            name.text = pers[(int)chara].Name;
            StartCoroutine(Sentense(eto));
        }
    }
    public void StartDialog(int m_number)
    {
        scene_number = m_number;
        dia.SetActive(true);
        tecFrase = scenes[scene_number].Frases[0];
        Say();
        active = true;

    }
    IEnumerator Sentense(string sentence)
    {
        CorutinWork = true;
        wind.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            wind.text += letter;
            yield return null;
        }
        CorutinWork = false;
    }

    public void CreateButton(string Ask, int point)
    {
        Button newBtn = Instantiate(btnPrefab, panel.transform, false);
        newBtn.GetComponentInChildren<Text>().text = Ask;
        newBtn.onClick.AddListener(delegate { Variant(point); });

        //foreach (Transform child in panel) Destroy(child.gameObject);

        
    }
    public void Variant(int point)
    {
        waiting = false;
        SledDialog();
        pers[0].sympaty += point;
        foreach (Transform child in panel) Destroy(child.gameObject);
    }

    

}