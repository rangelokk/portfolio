﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//[CreateAssetMenu(menuName = "Data/Dialog")]


[Serializable]
public class Dialog//: ScriptableObject
{
    public Charapter charapter;
    public Emotion emotion;
    public List<Reaction> reaction;
    public enum Charapter
    {
        Вопрос,
        Блэз,
        Сильвер,
        Райден,
        Соната,
        Хоширо,
        Реуль,
        Деми
    }
    public enum Emotion
    {
        Спокойствие,
        Радость,
        Интерес
    }
    [HideInInspector]
    public bool saved = false;
    [TextArea]
    public string text;
    [TextArea]
    public List<string> Variants;

    public List<Reaction> need;
}
[Serializable]
public struct Reaction
{
    public Vector2Int poits;
    public Dialog.Charapter charapter;
} 