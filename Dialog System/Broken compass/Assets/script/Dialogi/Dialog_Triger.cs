using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog_Triger : MonoBehaviour
{
    public int number_scene;
    public DialodSis dialodSis;
    public bool In;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (In && Input.GetKeyDown(KeyCode.Z))
        {
            if (!dialodSis.active) dialodSis.StartDialog(number_scene);
            else dialodSis.SledDialog();
        }
        if (dialodSis.active && Input.GetKeyDown(KeyCode.X)) dialodSis.SaveMemory();
    }
    void OnTriggerEnter2D (Collider2D collider)
    {
        //Debug.Log("ready");
        if (collider.gameObject.tag == "Player")
        {
            In = true;
        }
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        //Debug.Log("ready");
        if (collider.gameObject.tag == "Player")
        {
            In = false;
        }
    }
}
