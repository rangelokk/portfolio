using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rb;
    public Vector2 moveVelocity;
    Animator anima;
    public Vector2 remember;
    public DialodSis Dialog;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anima = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Dialog.active)
        {
            Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal") * 2, Input.GetAxisRaw("Vertical"));
            if (moveInput.x + moveInput.y != 0)
            {
                remember = moveInput;
                moveVelocity = moveInput * speed;
                if (moveInput.x < 0 && moveInput.y < 0) { anima.SetInteger("annim", 7); }
                if (moveInput.x > 0 && moveInput.y == 0) { anima.SetInteger("annim", 3); }
            }
            else
            {
                if (remember.x > 0) { anima.SetInteger("annim", 30); }
                //if (remember.x > 0 && remember.y == 0) { anima.SetInteger("annim", 30); }
                if (remember.x <= 0) { anima.SetInteger("annim", 70); }
                //if (remember.x < 0 && remember.y < 0) { anima.SetInteger("annim", 70); }
            }
            moveVelocity = moveInput * speed;
        }
        if (Input.GetKey(KeyCode.X)) time += Time.fixedDeltaTime;
        if (Input.GetKeyUp(KeyCode.X))
        {
            if (time > 3) Debug.Log("Long X");
            else Debug.Log("X");
                time = 0;
        }

    }
    float time = 0;
    void FixedUpdate()
    {
        
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
        

    }
   

}
