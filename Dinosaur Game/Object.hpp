
struct Point {
    int x;
    int y;
};
class Cactus1
{
public:

    Cactus1(int c_long, int c_high, int c_speed) {
        y = 400 - c_high;
        o_long = c_long;
        o_high = c_high;
        cactus = new sf::Sprite();
        o_long = c_long;
        o_high = c_high;
        speed = c_speed;
        cactus->setPosition(x, y);
    }
    ~Cactus1() {
        delete cactus;
    }

    void Move() {
        x -= speed;
        cactus->setPosition(x, y);
    }
    Point GetPoint() {
        Point p;
        p.x = x;
        p.y = y;
        return p;
    }
    sf::Sprite* Get() { return cactus; }
    int GetLong() {return o_long;}
private:
    sf::Sprite* cactus;
    int speed;
    int x = 900;
    int y = 400;
    int o_long;
    int o_high;
};

bool Collider(Point dino_p, Point Obj_p, int Obj_long) {
    if (dino_p.y >= Obj_p.y && ((dino_p.x >= Obj_p.x && dino_p.x - 40 <= Obj_p.x) || (dino_p.x >= Obj_p.x + Obj_long && dino_p.x - 40 <= Obj_p.x + Obj_long)))
        return false;
    return true;
}
