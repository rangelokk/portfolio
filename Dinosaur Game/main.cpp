﻿#include <SFML\Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <vector>
#include "Object.hpp"
#include <ctime> //для рандома

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
/////////////////
using namespace std::chrono_literals;
using namespace std;

bool Collider(Point dino_p, Point Obj_p, int Obj_long);

int x=200;
int y=400;
int speed = 0;
Point pd;
bool flag = true;
int timer=0;
int animation = 0;
int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 400), "Dinosaur from Chrome"); 
    //Загрузка текстур
    sf::Texture dinos;
    if (!dinos.loadFromFile("img/dino.png")) {
        cout << "No dino.png";
        return -10;
    }
    sf::Texture dinout;
    if (!dinout.loadFromFile("img/dino1.png")) {
        cout << "No dino1.png";
        return -2;
    }
    sf::Texture dinwalk1;
    if (!dinwalk1.loadFromFile("img/dino2.png")) {
        cout << "No dino1.png";
        return -3;
    }sf::Texture dinwalk2;
    if (!dinwalk2.loadFromFile("img/dino3.png")) {
        cout << "No dino3.png";
        return -4;
    }
    sf::Sprite dino(dinos);
    dino.setOrigin(70, 100);
    dino.setPosition(200, 400);

    sf::Texture textureb;
    if (!textureb.loadFromFile("img/backgroundColorDesert.png")) {
        cout << "No background";
        return -5;
    }
    sf::Image icon;
    if (!icon.loadFromFile("img/dinoi.png")) {
        cout << "No background";
        return -15;
    }
    window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
    sf::Sprite background(textureb);

    sf::Texture t_cactus;
    if (!t_cactus.loadFromFile("img/cactus1.png")) {
        cout << "No cactus1.png";
        return -6;
    }
    sf::Texture zabore;
    if (!zabore.loadFromFile("img/fence.png")) {
        cout << "No fence.png";
        return -8;
    }
    sf::Texture bush;
    if (!bush.loadFromFile("img/bush2.png")) {
        cout << "No bush2.png";
        return -7;
    }
    
    std::vector<Cactus1*> boxes;
    

    while (window.isOpen())
    {
        //Закрытие окна
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        //Появление препятствий
        timer++;
        if (timer % 40 == 0 && flag) {
           
            int type = rand() % 3;
            int speedi;
            srand(time(0));
            speedi = rand() % 5 + 5;
            switch (type)
            {
            case 0: {Cactus1* box = new Cactus1(70, 100, speedi);
                (*box->Get()).setTexture(t_cactus);
                boxes.push_back(box);}
                  break;
            case 1: {Cactus1* box = new Cactus1(70, 60, speedi);
                (*box->Get()).setTexture(zabore);
                boxes.push_back(box);}
                  break;
            case 2: {Cactus1* box = new Cactus1(53, 44, speedi);
                (*box->Get()).setTexture(bush);
                boxes.push_back(box);}
                  break;
            default:
                break;
            }
        }
        //Проверка столкнивений
        for (auto bOx : boxes)
        {
            if (Collider(pd, bOx->GetPoint(), bOx->GetLong())) flag = true;
            else {
                flag = false;  
                dino.setTexture(dinout);
                break;
            }
        }
        //Движения
        if (flag) {
            //Анимация динозаврика
            if (timer % 3 == 0) animation++;
            if (animation == 3) animation = 0;
            switch (animation) {
            case 0: dino.setTexture(dinos); break;
            case 1: dino.setTexture(dinwalk1); break;
            case 2: dino.setTexture(dinwalk2); break;
            default: break;
            }
            //Движение препятствий
            for (auto bOx : boxes) {
                bOx->Move();
            }
            //Прыжок
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && (y >= 390 && y <= 410)) {
                speed = 28;
            }
            y -= 1 * speed;
            if (speed > 0)speed--;
            if (y < 400) y += 10;
            dino.setPosition(x, y);
            pd.x = x;
            pd.y = y;
        }
        //Отрисовка
        window.clear();
        window.draw(background);
        window.draw(dino);
        for (auto bOx : boxes)
            if (bOx->Get())
                window.draw(*bOx->Get());
        window.display();
        std::this_thread::sleep_for(40ms);
    }
    //Очистка
    for (auto bOx : boxes)
    delete bOx;
    boxes.clear();
    return 0;
}