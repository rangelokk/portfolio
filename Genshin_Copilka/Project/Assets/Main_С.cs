using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class Main_� : MonoBehaviour
{
    public Image bar_main;
    public Image bar;
    public InputField input; 
    public int Summ;
    public int today;
    public float Goal;
    public float Today_Goal;
    public Text summ;
    public Text goal;
    public Text today_summ;
    public Text today_goal;
    public Text main_pr;
    public Text pr;
    public Text ost;

    public DateTime date;

    public Save sv = new Save();
    private string path;
    // Start is called before the first frame update
    private void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        path = Path.Combine(Application.persistentDataPath, "Save.json");
#else
        path = Path.Combine(Application.dataPath, "Save.json");
#endif
        if (File.Exists(path))
        {
            sv = JsonUtility.FromJson<Save>(File.ReadAllText(path));
            //Load
            Summ = sv.Summ;
            date = System.DateTime.Today;
            if (date.Day > DateTime.Parse(sv.date).Day)
            {
                today = 0;
            }
            else today = sv.todayy;
        }
        else
        {
            //Start Game
        }
       
    }
    /*
#if UNITY_ANDROID && !UNITY_EDITOR
private void OnApplicationPause(bool pause){
if(pause) {sv.Summ = Summ;  sv.todayy = today; sv.date = date.ToString(); File.WriteAllText(path, JsonUtility.ToJson(sv));}
}
#endif*/

    // Update is called once per frame
    void Update()
    {
        bar_main.fillAmount = Summ/Goal;
        //if(today / Today_Goal < 1)
        bar.fillAmount = today / Today_Goal;

        summ.text = Summ.ToString();
        goal.text = Goal.ToString();
        today_goal.text = Today_Goal.ToString();
        today_summ.text = today.ToString();
        pr.text = ((int)(today / Today_Goal*100)).ToString() + "%";
        main_pr.text = ((int)(Summ / Goal*100)).ToString() + "%";
        ost.text = "��������:" +(int)(Goal - Summ);
    }
    public void plus()
    {
        if (input.text != string.Empty)
        {
            Summ += int.Parse(input.text);
            today += int.Parse(input.text);
            input.text = string.Empty;
        }
    }
    public void minus()
    {
        if (input.text != string.Empty)
        {
            Summ -= int.Parse(input.text);
            today -= int.Parse(input.text);
            input.text = string.Empty;
        }
    }
    private void OnApplicationPause(bool pause)
    {
        sv.Summ = Summ;
        sv.todayy = today;
        sv.date = date.ToString();
        File.WriteAllText(path, JsonUtility.ToJson(sv));
    }
    private void OnApplicationQuit()
    {
        sv.Summ = Summ;
        sv.todayy = today;
        sv.date = date.ToString();
        File.WriteAllText(path, JsonUtility.ToJson(sv));
    }
}
[Serializable]
public class Save
{
    public int Summ;
    public string date;
    public int todayy;
}
