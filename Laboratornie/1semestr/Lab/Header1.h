#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int a[100][100];
int n;
void sort() {
    int p[100];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            p[i] *= a[i][j];
    for (int i = 0; i < n; i++)
        for (int r = i + 1; r < n; r++) {
            if (p[i] < p[r]) {
                for (int j = 0; j < n; j++)
                    swap(a[i][j], a[r][j]);
                swap(p[r], p[i]);
            }
        }

}
float absvel(int i, int j) {
    float d = i * i + j * j;
    d = sqrt(d);
    if (round(d) == d && (n != 1)) return d;  //���� ������� �� ������� �� 1 ��. �� � ��� ���� 2 �������� � ����������� ���������� 1  
    else return 0;                             //��������� ��� ��������: ����������� �� �����
}
bool findmin() {
    int mi = INT_MAX, k = 0;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++) {
            if (a[i][j] < mi) {
                mi = a[i][j]; k = 0;
            }
            else if (a[i][j] == mi) k++;
        }
    return (k > 0);
}
void read() {
    ifstream fin;
    fin.open("1.txt");
    fin >> n;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            fin >> a[i][j];
}

void write() {
    ofstream fout;
    fout.open("2.txt");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            fout << a[i][j] << " ";
        }
        fout << endl;
    }
    //cout << findmin()<<endl;
    //cout << absvel(3, 4);
}
