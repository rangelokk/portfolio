/*Вариант-4.
3
1. Дана последовательность натуральных чисел {aj}j=1...n (n<=10000). Если в последовательности нет ни одного простого числа, упорядочить последовательность по
невозрастанию.

2. Ввести последовательность натуральных чисел {Aj}j=1...n (n<=1000). Упорядочить последовательность по неубыванию первой цифры числа, числа с одинаковыми
первыми цифрами дополнительно упорядочить по неубыванию наименьшей цифры числа, числа с одинаковыми первыми цифрами и одинаковыми наименьшими цифрами
дополнительно упорядочить по неубыванию самого числа.

3. Дана целочисленная матрица {Aij}i=1..n,j=1..m (n,m<=100). Найти строку с наименьшей суммой элементов и заменнить все элементы этой строки этой суммой.*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Rus");

	int n, m;
	cin >> n >> m;
	int** a = new int*[n];
	for (int i = 0; i < n; i++)
		a[i] = new int[m];
	//int a[100][100];
	int sum = 0, minsum = 100000000, ind = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cin >> a[i][j];
			sum += a[i][j];
		}
		if (sum < minsum) {
			minsum = sum;
			ind = i;
		}
	}
	for (int j = 0; j < m; j++)
		a[ind][j] = minsum;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	for(int i = 0;  i<n; i++)
	delete[] a[i];
	delete[] a;


}
