#include <string>
using namespace std;

class MainGenerator {
public:

    virtual string New_Code() {return "new code";}
};

class CS : public MainGenerator { 

public:
    string New_Code() {return "new C# code";}
};

class Cpp : public MainGenerator {

public:
    string New_Code() {return "new C++ code";}
};

class Python : public MainGenerator {

public:
    string New_Code() {return "new Python code";}
};

class CodeGenerator {
public:

    CodeGenerator(MainGenerator* language) {
        _language = language;
    }

    string generateCode() {
        return _language->New_Code();
    }
private:
    MainGenerator* _language;
};






