#include <iostream>
#include <Generator.hpp>
using namespace std;

int main() {

    CS cs_code;
    Cpp cpp_code;
    Python python_code;

    CodeGenerator generatorCS = CodeGenerator( &cs_code );
    cout << generatorCS.generateCode() << endl;

    CodeGenerator generatorCpp = CodeGenerator( &cpp_code );
    cout << generatorCpp.generateCode() << endl;

    CodeGenerator generatorPython = CodeGenerator( &python_code );
    cout << generatorPython.generateCode() << endl;

    return 0;
}