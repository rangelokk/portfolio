﻿#include <iostream>
#include <cassert>
#include "inc/matrix.hpp"

using mt::math::Mat22d;
using mt::math::Mat33d;

using mt::math::Vec2d;



int main()
{
	std::cout << "=== Test 0 ===" << std::endl;
	{
		Mat22d A({ {
			 {1,2},
			 {3,4}
		} });

		
		std::cout << A.inv() << std::endl;
		std::cout << "After inv in main" << std::endl;
		
	}
	
	std::cout << "=== Test 1 ===" << std::endl;

	{
		Mat22d A({ {
			 {1,1},
			 {1,1}
		} });

		Mat33d D({ {
			 {1,2, 0},
			 {1,3, 0},
			{2, 1, 1}
		} });

		
		assert(D.det() == 1);
		std::cout << "Determinant D" << std::endl;
		std::cout <<D<< std::endl;
		std::cout<<"= " << D.det()<< std::endl;
		
		assert(A.det() == 0);
		std::cout << "Determinant A" << std::endl;
		std::cout << A << std::endl;
		std::cout << "= " << A.det() << std::endl;

		std::cout << "Transponirovanie" << std::endl;

		std::cout << D.trans();
	}

	std::cout << "Done!" << std::endl;

	return 0;
}