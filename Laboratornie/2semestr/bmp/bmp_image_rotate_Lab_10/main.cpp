﻿#include <BMP.hpp>
#include <iostream>


int main()
{
    try
    {
        mt::images::BMP pic(6000, 20000);
        pic.Fill({255,0, 255});
       
        pic.Open("in.bmp");

        pic.Rotate(acos(-1) / 6);
        pic.DelArtefact();
        pic.Save("test.bmp");
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}