# Язык Java

## Осенний семестр 2023, бакалавриат, 3 курс

## Список лабораторных работ

| Тема                   | Лабораторная работа                     |
|------------------------|-----------------------------------------|
| Базовые конструкции    | [Лабораторная работа 1](lab1/README.md) |
| ООП                    | [Лабораторная работа 2](lab2/README.md) |
| Коллекции и Stream API | [Лабораторная работа 3](lab3/README.md) |
