package lab1;

public class Problem2 {

    /**
     * Метод segregateEvenAndOddNumbers разделяет четные и нечетные числа в массиве array, т.у. возвращает массив,
     * в котором сперва записаны все четные числа массива array в порядке их следования, а затем все нечетные числа
     * массива array в порядке их следования.
     *
     * @param array массив положительных чисел
     * @return массив с разделенными четными и нечетными числами
     *
     * ПРИМЕР:
     * Вход: array = [2, 1, 5, 6, 8]
     * Выход: [2, 6, 8, 1, 5]
     */
    public static int[] segregateEvenAndOddNumbers(int[] array) {
        int[] array2 = new int[array.length]; int a2 = 0;
        int[] array3 = new int[array.length]; int a3 = 0;
        for(int el: array){
            if(el%2==0){
                array2[a2] = el; a2++;
            }
            else{
                array3[a3] = el; a3++;
            }
        }
        for(int i = 0; i<array.length; i++){
            if(i<a2) array[i] = array2[i];
            else array[i] = array3[i-a2];
        }
        return array;
    }

}
