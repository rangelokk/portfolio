package lab1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Problem2Tests {
    boolean equal(int[] a1, int [] a2) {
        if(a1.length != a2.length) return false;
        for (int i = 0; i<a1.length; i++){
            if(a1[i] != a2[i]) return false;
        }
        return true;
    }
    @Test
    void Test1() {
        int[] a ={2, 1, 5, 6, 8};
        int[] ans = {2, 6, 8, 1, 5};
        int[] b = Problem2.segregateEvenAndOddNumbers(a);
        for (int el: b) System.out.println(el);
        assertTrue(equal(b, ans));
    }
    @Test
    void Test2() {
        int[] a ={1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] ans = {2, 4, 6, 8, 1, 3, 5, 7, 9};
        int[] b = Problem2.segregateEvenAndOddNumbers(a);
        for (int el: b) System.out.println(el);
        assertTrue(equal(b, ans));
    }
}
