package lab1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Problem3Tests {
    boolean equal(int[] a1, int [] a2) {
        if(a1.length != a2.length) return false;
        for (int i = 0; i<a1.length; i++){
            if(a1[i] != a2[i]) return false;
        }
    return true;
    }

    @Test
    void Test1() {
        // given
        int[][] matrix = {{1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}};

        // when
        int[] array = Problem3.flattenMatrix(matrix);
        boolean actual = false;
        int[] array2 = {1, 4, 7, 2, 5, 8, 3, 6, 9};
        // then
        assertTrue(equal(array, array2));
    }
    @Test
    void Test2() {
        // given
        int[][] matrix = {{1, 0, 0},
                        {0, 1, 0},
                        {0, 0, 1}};

        // when
        int[] array = Problem3.flattenMatrix(matrix);
        boolean actual = false;
        int[] array2 = {1, 0, 0, 0, 1, 0, 0, 0, 1};
        // then
        assertTrue(equal(array, array2));
    }


}
