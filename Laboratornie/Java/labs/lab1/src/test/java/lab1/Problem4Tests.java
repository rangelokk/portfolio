package lab1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Problem4Tests {

    @Test
    void Test1() {
        assertTrue(Problem4.isGeometricProgression("3, 1, 2"));
    }
    @Test
    void Test2() {
        assertTrue(Problem4.isGeometricProgression("-7, -4, -1, 2, 5"));
    }
    @Test
    void Test3() {
        assertFalse(Problem4.isGeometricProgression("-7, -4, 2, 5"));
    }



}
