package lab2;
import lab2.model.ActionStatus;
import lab2.model.CourseInstance;
import lab2.model.Instructor;
import lab2.model.Student;
import lab2.service.AdministrationService;
import lab2.service.StudentService1;
import lab2.service.CourseInstructorService1;
import java.io.IOException;



public class Main {
    public static AdministrationService administration = new AdministrationService();
    public static void main(String[] a) throws IOException {
        System.out.println("Меню");
        administration.Load();
        administration.showAllStudents();

        StudentService1 studentService1 = new StudentService1();
        if(studentService1.subscribe(103,100002) == ActionStatus.OK)
            System.out.println("Успешная запись на курс");
        else System.out.println("Не удалось записаться на курс");


        System.out.println("Список студентов на курсе номер 10123");
        CourseInstructorService1 instructorService1 = new CourseInstructorService1();
        Student[] s = instructorService1.findStudentsByCourseId(10123);
        for(Student student: s){
            System.out.println(student.getName());
        }

        for(CourseInstance course: studentService1.findAllSubscriptionsByStudentId(103)){
            System.out.println(course.getCourseId());
        }

        for(Student student: instructorService1.findStudentsByInstructorId(9002)){
            System.out.println(student.getName());
        }
        System.out.println("Преподаватели на замену:");
        for(Instructor instructor: instructorService1.findReplacement(9002, 10123)){
            System.out.println(instructor.getName());
        }
    }
}
