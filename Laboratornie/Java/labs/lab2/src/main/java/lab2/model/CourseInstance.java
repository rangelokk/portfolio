package lab2.model;

import java.time.LocalDate;

/**
 * Класс с данными о проведении курса. Один курс (например, дискретная математика) может быть проведен несколько раз.
 */
public class CourseInstance {
    
    /**
     * идентификатор проведения курса
     */
    private long id;

    /**
     * идентификатор курса, соответствующий CourseInfo.id
     */
    private long courseId;

    /**
     * идентификатор преподавателя
     */
    private long instructorId;

    /**
     * дата начала курса
     */
    private LocalDate startDate;

    /**
     * ограничение на число студентов курса
     */
    private int capacity;

    private int[] studentsOnCourse;

    public int[] getStudentsOnCourse(){
        int[] a = {};
        if(studentsOnCourse==null) return a;
        return this.studentsOnCourse;
    }

    public int getCapacity() {
        return capacity;
    }

    public long getCourseId() {
        return courseId;
    }

    public long getInstructorId() {
        return instructorId;
    }

    public long getId() {
        return id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setInstructorId(long instructorId) {
        this.instructorId = instructorId;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setStartDate(String startDate) {
        this.startDate = LocalDate.parse(startDate);
    }

    public void setStudentsOnCourse(int[] studentsOnCourse) {
        this.studentsOnCourse = studentsOnCourse;
    }
}
