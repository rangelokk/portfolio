package lab2.reader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lab2.model.CourseInfo;
import lab2.model.CourseInstance;
import lab2.model.Instructor;
import lab2.model.Student;
import java.time.LocalDate;

import java.io.File;
import java.io.IOException;
public class CouseDataReader {
    private ObjectMapper objectMapper = new ObjectMapper();
    public CourseInfo[] readCourseInfoData() throws IOException {
        return objectMapper.readValue(new File("src/main/resources/courseInfos.json"), CourseInfo[].class);
    }
    public CourseInstance[] readCourseInstanceData() throws IOException {
        return objectMapper.readValue(new File("src/main/resources/courseInstances.json"), CourseInstance[].class);
    }
    public void setCourseInstanceData(CourseInstance[] courseInstances) throws IOException{
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        String s = objectMapper.writeValueAsString(courseInstances);

    }
    public Instructor[] readInstructorData() throws IOException{
        return objectMapper.readValue(new File("src/main/resources/instructors.json"), Instructor[].class);
    }
}
