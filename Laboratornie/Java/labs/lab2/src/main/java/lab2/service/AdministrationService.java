package lab2.service;

import lab2.model.CourseInfo;
import lab2.model.CourseInstance;
import lab2.model.Instructor;
import lab2.model.Student;
import lab2.reader.StudentDataReader;
import lab2.reader.CouseDataReader;
import java.io.IOException;


public class AdministrationService {
    public static Student[] studentsB;
    public static Student[] studentsM;
    public static CourseInstance[] coursesInstance;
    public static CourseInfo[] coursesInfo;

    public static Instructor[] instructors;

    public static void Load() throws IOException {
        StudentDataReader reader = new StudentDataReader();
        studentsB = reader.readBachelorStudentData();
        studentsM = reader.readMasterStudentData();

        CouseDataReader reader2 = new CouseDataReader();
        coursesInstance = reader2.readCourseInstanceData();
        coursesInfo = reader2.readCourseInfoData();
        instructors = reader2.readInstructorData();
    }
    public static void showAllStudents() throws IOException {
        if (studentsB == null) Load();
        System.out.println("Список студентов:");
        for (int i = 0; i < studentsB.length; i++) {
            System.out.println(studentsB[i].getName());
        }
    }
    public static CourseInstance[] getCouseInstanceByIdCourse(long courseId) {
        CourseInstance[] instance = new CourseInstance[coursesInstance.length];
        int n = 0;
        for(CourseInstance ins: coursesInstance){
            if(ins.getCourseId() == courseId) {
                instance[n] = ins;
                n++;
            }
        }

        CourseInstance[] ins2 = new CourseInstance[n];
        for(int i =0; i<n; i++){
            ins2[i] = instance[i];
        }
        return ins2;
    }
    public static CourseInfo getCourseInfoById(long courseId){
        for(CourseInfo info: coursesInfo){
            if(info.getId() == courseId) return info;
        }
        return null;
    }
    public static CourseInstance getCourseInstanceById(long courseId){
        for(CourseInstance info: coursesInstance){
            if(info.getId() == courseId) return info;
        }
        return null;
    }
    public static Student getStudentMById (long Id) {
        for(Student student: studentsM){
            if(student.getId() == Id) return student;
        }
        return null;
    }
    public static Student getStudentBById (long Id) {
        for(Student student: studentsB){
            if(student.getId() == Id) return student;
        }
        return null;
    }
    public int[] plus(int[] a1, int[] a2){
        int[] a3 = new int[a1.length + a2.length];
        for(int i = 0; i<a1.length; i++) a3[i] = a1[i];
        for(int i = 0; i<a2.length; i++) a3[i + a1.length] = a2[i];
        return a3;
    }

}
