package lab2.service;

import lab2.Main;
import lab2.model.CourseInstance;
import lab2.model.Instructor;
import lab2.model.Student;

public class CourseInstructorService1 implements CourseInstructorService{
    AdministrationService administration = Main.administration;


    @Override
    public Student[] findStudentsByCourseId(long courseId) {
        CourseInstance course[] = administration.getCouseInstanceByIdCourse(courseId);
        int[] studentsId = new int[0];
        for(CourseInstance instance: course) studentsId = administration.plus(studentsId, instance.getStudentsOnCourse());
        Student[] students1 = new Student[studentsId.length];
        for(int i = 0; i<studentsId.length; i++) {
            if (administration.getStudentMById(studentsId[i])!= null) {
                students1[i] = administration.getStudentMById(studentsId[i]);
            }
            if (administration.getStudentBById(studentsId[i])!= null) {
                students1[i] = administration.getStudentBById(studentsId[i]);
            }
        }
        return students1;
    }

    @Override
    public Student[] findStudentsByInstructorId(long instructorId) {
        int[] studentsId = new int[0];
        for(CourseInstance course: administration.coursesInstance){
            if(course.getInstructorId() == instructorId){

                for(int stud: course.getStudentsOnCourse()) {
                    boolean flag = true;
                    for (int st : studentsId) {
                        if (stud == st) {
                            flag = false;
                            break;
                        }
                    }
                    int[] a= {stud};
                    if(flag) studentsId = administration.plus(studentsId, a);
                }
            }
        }
        Student[] students = new Student[studentsId.length];
        for(int i =0; i<studentsId.length; i++){
            if(administration.getStudentMById(studentsId[i])!=null)
            students[i] = administration.getStudentMById(studentsId[i]);
            else if(administration.getStudentBById(studentsId[i])!=null)
                students[i] = administration.getStudentBById(studentsId[i]);
        }
        return students;
    }

    @Override
    public Instructor[] findReplacement(long instructorId, long courseId) {
        Instructor instructor[] = new Instructor[administration.instructors.length];
        int n = 0;
        for(Instructor instructor1: administration.instructors) {
            for(int courses :instructor1.getCanTeach()){
                if(courses == courseId && instructor1.getId() != instructorId) {
                    instructor[n] = instructor1;
                    n++;
                }
            }
        }
        Instructor[] instructors2 = new Instructor[n];
        for(int i = 0; i<n; i++){
            instructors2[i] = instructor[i];
        }
        return instructors2;
    }
}
