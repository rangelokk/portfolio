package lab2.service;

import lab2.Main;
import lab2.model.*;

import java.time.LocalDate;

public class StudentService1 implements StudentService{
    AdministrationService administration = Main.administration;
    @Override
    public ActionStatus subscribe(long studentId, long courseId) {
        if (administration.getCourseInstanceById(courseId) == null) {
            System.out.println("Не найдены записи курса");
            return ActionStatus.NOK;
        }
        CourseInstance course = administration.getCourseInstanceById(courseId);
        StudentCategory category;
        Student student;
        if (administration.getStudentMById(studentId) != null) {
            category = StudentCategory.MASTER;
            student = administration.getStudentMById(studentId);
        } else if (administration.getStudentBById(studentId) != null) {
            category = StudentCategory.BACHELOR;
            student = administration.getStudentBById(studentId);
        } else {
            System.out.println("Студент не найден");
            return ActionStatus.NOK;
        }
        CourseInfo courseInfo;
        if (administration.getCourseInfoById(course.getCourseId()) != null)
            courseInfo = administration.getCourseInfoById(course.getCourseId());
        else {
            System.out.println("Не найдена информация о курсе");
            return ActionStatus.NOK;
        }
        if (course.getStudentsOnCourse().length >= course.getCapacity() && course.getCapacity() != 0) {
            System.out.println("Мест нет");
            return ActionStatus.NOK;
        }
        if (LocalDate.now().isAfter(course.getStartDate())) {
            System.out.println("Курс уже начался/прошел");
            return ActionStatus.NOK;
        }
        boolean flag = false;
        for (StudentCategory requaredCategory : courseInfo.getStudentCategories()) {
            if (requaredCategory == category) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            System.out.println("Неподходящая категория");
            return ActionStatus.NOK;
        }

        for (int students : course.getStudentsOnCourse()) {
            if (students == studentId) {
                System.out.println("Студент уже записан на курс");
                return ActionStatus.NOK;
            }
        }
        if(courseInfo.getPrerequisites()!=null)
        for (long requred : courseInfo.getPrerequisites()) {
            boolean flag2 = false;
            if(student.getCompletedCourses()!=null)
            for (long complete : student.getCompletedCourses()) {
                if (requred == complete) flag2 = true;
            }
            if (!flag2) {
                System.out.println("Не пройдены необходимые курсы");
                return ActionStatus.NOK;
            }
        }

            int[] a = {(int) studentId};
            course.setStudentsOnCourse(administration.plus(course.getStudentsOnCourse(), a));

            return ActionStatus.OK;

    }

    @Override
    public ActionStatus unsubscribe(long studentId, long courseId) {
        Student student;
        if (administration.getStudentMById(studentId) != null) {
            student = administration.getStudentMById(studentId);
        } else if (administration.getStudentBById(studentId) != null) {
            student = administration.getStudentBById(studentId);
        } else {
            System.out.println("Студент не найден");
            return ActionStatus.NOK;
        }
        if (administration.getCourseInstanceById(courseId) == null) {
            System.out.println("Не найдены записи курса");
            return ActionStatus.NOK;
        }
        CourseInstance course = administration.getCourseInstanceById(courseId);
        if (LocalDate.now().isAfter(course.getStartDate())) {
            System.out.println("Курс уже начался/прошел");
            return ActionStatus.NOK;
        }
        int[] students = new int[course.getStudentsOnCourse().length-1];
        int n = 0;
        for(int st: course.getStudentsOnCourse()){
            if(st!=studentId)
            students[n] = st;
        }
        course.setStudentsOnCourse(students);
        return ActionStatus.OK;
    }

    @Override
    public CourseInstance[] findAllSubscriptionsByStudentId(long studentId) {
        int n =0;
        CourseInstance[] courseInstance = new CourseInstance[administration.coursesInstance.length];
        for(CourseInstance instance: administration.coursesInstance) {
            for (int student : instance.getStudentsOnCourse()) {
                if (student == studentId) {
                    courseInstance[n] = instance;
                    n++;
                }
            }
        }
        CourseInstance[] courseInstances = new CourseInstance[n];
        for(int i = 0; i<n; i++){
            courseInstances[i] = courseInstance[i];
        }
        return courseInstances;
    }
}
