package lab3.model;

/**
 * Класс для информации о студенте
 */
public class Student extends Person {

    /**
     * список идентификаторов курсов (CourseInstance.id), пройденных студентом
     */
    private long[] completedCourses;
    public long[] getCompletedCourses() {return this.completedCourses;}
    public void setCompletedCourses(long[] m_completedCourses) {this.completedCourses = m_completedCourses;}
    
}
