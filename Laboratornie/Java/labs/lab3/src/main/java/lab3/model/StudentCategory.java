package lab3.model;

/**
 * Enum для категорий студентов
 */
public enum StudentCategory {
    BACHELOR,
    MASTER
}
