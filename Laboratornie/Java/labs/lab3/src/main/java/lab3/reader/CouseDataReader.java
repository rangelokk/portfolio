package lab3.reader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lab3.model.CourseInfo;
import lab3.model.CourseInstance;
import lab3.model.Instructor;
import lab3.model.Student;
import java.time.LocalDate;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CouseDataReader {
    private ObjectMapper objectMapper = new ObjectMapper();
    public List<CourseInfo> readCourseInfoData() throws IOException {
        return Arrays.stream(objectMapper.readValue(new File("src/main/resources/courseInfos.json"), CourseInfo[].class)).toList();
    }
    public List<CourseInstance> readCourseInstanceData() throws IOException {
        return Arrays.stream(objectMapper.readValue(new File("src/main/resources/courseInstances.json"), CourseInstance[].class)).toList();
    }
    public void setCourseInstanceData(CourseInstance[] courseInstances) throws IOException{
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        String s = objectMapper.writeValueAsString(courseInstances);

    }
    public List<Instructor> readInstructorData() throws IOException{
        return Arrays.stream(objectMapper.readValue(new File("src/main/resources/instructors.json"), Instructor[].class)).toList();
    }
}
