package lab3.service;

import lab3.model.*;
import lab3.reader.StudentDataReader;
import lab3.reader.CouseDataReader;
import java.io.IOException;
import java.util.List;


public class AdministrationService {
    public static List<Student> studentsB;
    public static List<Student> studentsM;
    public static List<CourseInstance> coursesInstance;
    public static List<CourseInfo> coursesInfo;

    public static List<Instructor> instructors;

    public static void Load() throws IOException {
        System.out.println("Произошла загрузка");
        StudentDataReader reader = new StudentDataReader();
        studentsB = reader.readBachelorStudentData();
        studentsM = reader.readMasterStudentData();

        CouseDataReader reader2 = new CouseDataReader();
        coursesInstance = reader2.readCourseInstanceData();
        coursesInfo = reader2.readCourseInfoData();
        instructors = reader2.readInstructorData();
    }
    public static void showAllStudents() throws IOException {
        if (studentsB == null) Load();
        System.out.println("Список студентов:");
        studentsB.forEach(student -> {System.out.println(student.getName());});
    }
    public static List<CourseInstance> getCouseInstanceByIdCourse(long courseId) {
        return coursesInstance.stream()
                .filter(student -> student.getCourseId() == courseId).toList();
    }
    public static CourseInfo getCourseInfoById(long courseId){
        if(coursesInfo.stream().anyMatch(info -> info.getId() == courseId))
        return coursesInfo.stream()
                .filter(info -> info.getId() == courseId).findAny().get();
        else return null;
    }
    public static CourseInstance getCourseInstanceById(long courseId){
        if(coursesInstance.stream().anyMatch(info -> info.getId() == courseId))
        return coursesInstance.stream()
                .filter(info -> info.getId() == courseId).findAny().get();
        else return null;
    }
    public static Student getStudentMById (long Id) {
        if(studentsM.stream().anyMatch(info -> info.getId() == Id))
        return studentsM.stream()
                .filter(info -> info.getId() == Id).findAny().get();
        else return null;
    }
    public static Student getStudentBById (long Id) {
        if(studentsB.stream().anyMatch(info -> info.getId() == Id))
        return studentsB.stream()
                .filter(info -> info.getId() == Id).findAny().get();
        else return null;
    }
    /*public int[] plus(int[] a1, int[] a2){
        int[] a3 = new int[a1.length + a2.length];
        for(int i = 0; i<a1.length; i++) a3[i] = a1[i];
        for(int i = 0; i<a2.length; i++) a3[i + a1.length] = a2[i];
        return a3;
    }*/

}
