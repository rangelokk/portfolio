package lab3.service;

import lab3.Main;
import lab3.model.CourseInstance;
import lab3.model.Instructor;
import lab3.model.Student;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CourseInstructorService1 implements CourseInstructorService{
    AdministrationService administration = Main.administration;


    @Override
    public Student[] findStudentsByCourseId(long courseId) {
        List<CourseInstance> course = administration.getCouseInstanceByIdCourse(courseId);

        ArrayList<Integer> studentsId = new ArrayList<Integer>();
        course.forEach(instance ->
        studentsId.addAll(Arrays.stream(instance.getStudentsOnCourse()).boxed().toList()));

        ArrayList<Student> students= new ArrayList<Student>();
        students.addAll(studentsId.stream().map(st->administration.getStudentMById(st)!=null?
                administration.getStudentMById(st):
                administration.getStudentBById(st)).toList());

        return students.toArray(Student[]::new);
    }

    @Override
    public Student[] findStudentsByInstructorId(long instructorId) {
        ArrayList<Integer> studentsId = new ArrayList<Integer>();

        administration.coursesInstance.stream()
                .filter(course->course.getInstructorId() == instructorId)
                .forEach(course->{
                    Arrays.stream(course.getStudentsOnCourse()).forEach(stud->{
                        if(!studentsId.stream().anyMatch(st->stud == st))
                                studentsId.add(stud);
                        });
                });
    ArrayList<Student> students= new ArrayList<Student>();
        students.addAll(studentsId.stream().map(st->administration.getStudentMById(st)!=null?
            administration.getStudentMById(st):
            administration.getStudentBById(st)).toList());

        return students.toArray(Student[]::new);
    }

    @Override
    public Instructor[] findReplacement(long instructorId, long courseId) {
        return administration.instructors.stream()
                .filter(instructors -> Arrays.stream(instructors.getCanTeach())
                        .anyMatch(courses -> courses == courseId)&&instructors.getId()!=instructorId)
                                .toArray(Instructor[]::new);
    }


}


