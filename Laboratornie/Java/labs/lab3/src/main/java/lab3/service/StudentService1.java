package lab3.service;

import lab3.Main;
import lab3.model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StudentService1 implements StudentService{
    AdministrationService administration = Main.administration;
    @Override
    public ActionStatus subscribe(long studentId, long courseId) {
        if (administration.getCourseInstanceById(courseId) == null) {
            System.out.println("Не найдены записи курса");
            return ActionStatus.NOK;
        }
        CourseInstance course = administration.getCourseInstanceById(courseId);
        StudentCategory category;
        Student student;
        if (administration.getStudentMById(studentId) != null) {
            category = StudentCategory.MASTER;
            student = administration.getStudentMById(studentId);
        } else if (administration.getStudentBById(studentId) != null) {
            category = StudentCategory.BACHELOR;
            student = administration.getStudentBById(studentId);
        } else {
            System.out.println("Студент не найден");
            return ActionStatus.NOK;
        }
        CourseInfo courseInfo;
        if (administration.getCourseInfoById(course.getCourseId()) != null)
            courseInfo = administration.getCourseInfoById(course.getCourseId());
        else {
            System.out.println("Не найдена информация о курсе");
            return ActionStatus.NOK;
        }
        if (course.getStudentsOnCourse().length >= course.getCapacity() && course.getCapacity() != 0) {
            System.out.println("Мест нет");
            return ActionStatus.NOK;
        }
        if (LocalDate.now().isAfter(course.getStartDate())) {
            System.out.println("Курс уже начался/прошел");
            return ActionStatus.NOK;
        }
        if(!Arrays.stream(courseInfo.getStudentCategories())
                .anyMatch(requaredCategory -> requaredCategory == category)){
            System.out.println("Неподходящая категория");
            return ActionStatus.NOK;
        }

        List<Integer> StudentsOnCourse = Arrays.stream(course.getStudentsOnCourse()).boxed().toList();
        if (StudentsOnCourse.stream().anyMatch(Id->Id==studentId)) {
            System.out.println("Студент уже записан на курс");
            return ActionStatus.NOK;
        }
        if(courseInfo.getPrerequisites()!=null)
        if(Arrays.stream(courseInfo.getPrerequisites())
                .anyMatch(requared ->
                        !Arrays.stream(student.getCompletedCourses())
                                .anyMatch(complete -> complete == requared))){
            System.out.println("Не пройдены необходимые курсы");
            return ActionStatus.NOK;
        }
        Integer a = (int) studentId;
        StudentsOnCourse = new ArrayList<Integer>(StudentsOnCourse);
        StudentsOnCourse.add(a);
        course.setStudentsOnCourse(StudentsOnCourse.stream().mapToInt(i->i).toArray());
            return ActionStatus.OK;

    }

    @Override
    public ActionStatus unsubscribe(long studentId, long courseId) {
        Student student;
        if (administration.getStudentMById(studentId) != null) {
            student = administration.getStudentMById(studentId);
        } else if (administration.getStudentBById(studentId) != null) {
            student = administration.getStudentBById(studentId);
        } else {
            System.out.println("Студент не найден");
            return ActionStatus.NOK;
        }
        if (administration.getCourseInstanceById(courseId) == null) {
            System.out.println("Не найдены записи курса");
            return ActionStatus.NOK;
        }
        CourseInstance course = administration.getCourseInstanceById(courseId);
        if (LocalDate.now().isAfter(course.getStartDate())) {
            System.out.println("Курс уже начался/прошел");
            return ActionStatus.NOK;
        }
        if(Arrays.stream(course.getStudentsOnCourse()).anyMatch(Id -> studentId == Id)){
            course.setStudentsOnCourse(Arrays.stream(course.getStudentsOnCourse())
                    .filter(stud -> stud!=studentId).toArray());
        }
        return ActionStatus.OK;
    }

    @Override
    public CourseInstance[] findAllSubscriptionsByStudentId(long studentId) {
        return administration.coursesInstance.stream()
                .filter(instance -> Arrays.stream(instance.getStudentsOnCourse())
                        .anyMatch(student -> student == studentId)).toArray(CourseInstance[]::new);
    }
}
