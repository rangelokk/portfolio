#include <SFML\Graphics.hpp>
#include <vector>
#include <fstream>
#include <memory>
#include <iostream>
#include "Pokemon.h"


sf::Event event;
int Scene = 0;
int Back;
int Our_Pokemon = 0;

class Window {

public: 
	virtual void Play(sf::RenderWindow& window) {}
	virtual void Load() {}
	virtual void Text_Enter(sf::RenderWindow& window) {}
	Player* player;
	  
};


struct Button {
	Button(int X, int Y, float Wight, float High, std::string sign) {

		
		rect.setSize(sf::Vector2f(Wight, High));
		rect.setOrigin(Wight / 2, High / 2);
		rect.setPosition(X, Y);
		if (sign != "") {
			rect.setFillColor(sf::Color(100, 250, 50));
			arial.loadFromFile("img/Kenney Future Narrow.ttf");
			text.setFillColor(sf::Color::Black);
			text.setFont(arial);
			text.setString(sign);
			text.setOrigin((text.findCharacterPos(sign.size() - 1).x - text.findCharacterPos(0).x) / 2, (text.getCharacterSize() + text.findCharacterPos(0).y) / 2 + 5);
		}
		else rect.setFillColor(sf::Color(0, 0, 0, 0));
		
		
		text.setPosition(X, Y);
		x = X;
		y = Y;
		w = Wight;
		h = High;
		r = sign;
	}
	bool Draw(sf::RenderWindow& window) {
		bool flag = false;
		window.draw(rect);
		if (sf::IntRect(x - w / 2, y - h / 2, w, h).contains(sf::Mouse::getPosition(window)))
		{
			if (r != "") rect.setFillColor(sf::Color(74, 73, 107));
			else rect.setFillColor(sf::Color(0, 0, 0, 40));
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) flag = true;
		}
		else if (r != "")rect.setFillColor(sf::Color(115, 105, 132));
		else rect.setFillColor(sf::Color(0, 0, 0, 0));
		window.draw(text);
		return flag;
	}
	sf::RectangleShape rect;
	int x;
	int y;
	float w;
	float h;

	sf::Font arial;
	sf::Text text;
	std::string r = "";
};

class Start_Window: public Window {
public:
	Start_Window() {
		button_start = new Button(400, 270, 300, 50, "Start new game");
		button_load = new Button(400, 370, 300, 50, "Load game");
		std::cout << "load start";
		if (!menu_tex.loadFromFile("img/menu.png")) {
			std::cout << "No png";
		}
		menu.setTexture(menu_tex);
		//button_start->setFuntion([] {Scene = 1;});
	}
    virtual void Play(sf::RenderWindow& window) {
		
		window.clear(sf::Color::Black);
		window.draw(menu);
		if (button_start->Draw(window)) {
			Scene = 1;
			player->my_team.clear();
			player->Join_the_Team(new Eevee(5));
		}
		if (button_load->Draw(window))
		{
			player->Load(); Scene = 4;
		}
		window.display();
		
	}
	~Start_Window() {
		delete button_start;
		delete button_load;
		std::cout << " delete start ";
	}
	std::string r = "Start Game \n Press Enter";
	Button* button_start;
	Button* button_load;
	sf::Sprite menu;
	sf::Texture menu_tex;
};

struct Hp_Bar {

	sf::RectangleShape rect;

	Hp_Bar(int X, int Y, float Wight, float High) {
		rect.setSize(sf::Vector2f(Wight, High));
		rect.setPosition(X, Y);
		rect.setFillColor(sf::Color::Green);
		w = Wight;
		h = High;
	}
	void Draw(sf::RenderWindow& window, float Hp, float max_Hp) {

		rect.setSize(sf::Vector2f(w * Hp / max_Hp, h));
		if(Hp / max_Hp < 0.2) rect.setFillColor(sf::Color::Red);
		else if (Hp / max_Hp < 0.5) rect.setFillColor(sf::Color::Yellow);
		else rect.setFillColor(sf::Color::Green);
		window.draw(rect);
		


	}
	float w;
	float h;
};

struct Textes {
	Textes(int x, int y) {
		arial.loadFromFile("img/Kenney Future Narrow.ttf");
		text.setFillColor(sf::Color::Black);
		text.setFont(arial);
		text.setPosition(x, y);
		text.setCharacterSize(50);
	}

	void Show(sf::RenderWindow& window) {
		if (all != "" && i < all.size()) {
			show += all[i];
			i++;
		}
		text.setString(show);
		window.draw(text);
	}
	std::string show = "";
	std::string all;
	int i = 0;
	sf::Text text;
	sf::Font arial;
};



class Pokemon_Inventory : public Window {
public:
	Pokemon_Inventory() {
		if (!back.loadFromFile("img/okno.png")) {
			std::cout << "No png";
		}
		backgr.setTexture(back);
		stats = new Textes(100, 300);
		stats->text.setCharacterSize(50);
		rect.setSize(sf::Vector2f(205, 40));
		rect.setPosition(180, 370);
		rect.setFillColor(sf::Color(32, 53, 80));
	}
	void Play(sf::RenderWindow& window) {
		window.clear(sf::Color::Black);
		window.draw(backgr);
		if (Back == 1) {
			Load();
			Back = 0;
		}
		//oneb->Draw(window);
		for (int i=0; i < player->my_team.size(); i++) 
		window.draw(icon[i]);
		
		for (int i = 0; i < player->my_team.size(); i++)
		if (oneb[i]->Draw(window))
		{
			
			select.setTexture(player->my_team[i].get()->poketex);
			std::string r = { "Lv " + std::to_string(player->my_team[i].get()->LV) + "\nHp " + std::to_string(player->my_team[i].get()->hp) + "/" + std::to_string(player->my_team[i].get()->max_Hp) +
			"\nAtack " + std::to_string(player->my_team[i].get()->atack) + "\nDef" + std::to_string(player->my_team[i].get()->def) };
			stats->show = "";
			stats->i = 0;
			stats->all = r;
			selected = i;
		}
		for (int i = 0; i < player->my_team.size(); i++) names[i]->Show(window); 
		if (selected != -1) {
			window.draw(select);
			window.draw(rect);
			bar->Draw(window, player->my_team[selected]->hp, player->my_team[selected]->max_Hp);
			stats->Show(window);
			if (backB->Draw(window))
			{
				if (player->my_team[selected]->hp != 0)
				{
					Scene = 2; Our_Pokemon = selected; selected = -1;
				}
			}
		}
		window.display();
	}
	void Load() {
		for (int i = 0; i < player->my_team.size(); i++) {
			icon[i].setTexture(player->my_team[i].get()->poketex3);
			icon[i].setPosition(480, 5 + i*105);
			icon[i].setScale(sf::Vector2f(4.0, 4.0));
		}
		for (int i = 0; i < 5; i++) {
			names[i] = new Textes(600, 47 + i * 105);
		}
		for (int i = 0; i < player->my_team.size(); i++) {
			names[i]->all = player->my_team[i].get()->name;
		}
		for (int i = 0; i < player->my_team.size(); i++)
		oneb[i] = new Button(815, 87 + i * 105, 680, 80, "");
		backB = new Button(260, 70, 210, 80, "Select");
	
		select.setScale(sf::Vector2f(4.0, 4.0));
		select.setPosition(100, 50);
		bar = new Hp_Bar(180, 370, 205, 40);
	}
	~Pokemon_Inventory() {
		for (int i = 0; i < player->my_team.size(); i++)
		{
			delete oneb[i];
			delete names[i];
		}
		delete backB;
		delete stats;
		delete bar;
		std::cout << " delete inventory";
	}
	sf::Texture back;
	sf::Sprite backgr;
	sf::Sprite icon[5];
	sf::Sprite select;
	sf::RectangleShape rect;
	Button* oneb[5];
	Textes* names[5];
	Button* backB;
	Textes* stats;
	Hp_Bar* bar;
	int selected = -1;
};

class Battle_Window : public Window {
public:
	
	Battle_Window() {
		if (!back.loadFromFile("img/bg.png")) {
			std::cout << "No png";
		}
		backgr.setTexture(back);
		
		enemy_Hp = new Hp_Bar(247, 97, 231, 15);
		my_Hp = new Hp_Bar(833, 374, 231, 15);


		arial.loadFromFile("img/Kenney Future Narrow.ttf");
		enemy_name.setFillColor(sf::Color::Black);
		enemy_name.setFont(arial);
		enemy_name.setPosition(90, 35);
		enemy_name.setCharacterSize(35);

		my_name.setFillColor(sf::Color::Black);
		my_name.setFont(arial);
		my_name.setPosition(17*40, 8*40);
		my_name.setCharacterSize(35);

		attack = new Button(730, 550, 235, 70, "Attack");
		Run = new Button(985, 635, 235, 70, "Run");
		pokeball = new Button(985, 550, 235, 70, "Catch");
		pokemon = new Button(730, 635, 235, 70, "Pokemon");
		
		text = new Textes(45, 505);
		
	}
	void Load() {
		Our_Pokemon = 0;
		srand(time(0));
		int pok = rand() % 4;
		switch (pok) {
		case 0: wild_Pokemon = new Eevee(rand () % player->my_team[Our_Pokemon].get()->LV + 5); break;
		case 1: wild_Pokemon = new Flareon(rand() % player->my_team[Our_Pokemon].get()->LV); break;
		case 2: wild_Pokemon = new Vaporeon(rand() % player->my_team[Our_Pokemon].get()->LV); break;
		case 3: wild_Pokemon = new Jolteon(rand() % player->my_team[Our_Pokemon].get()->LV); break;
		}
		enemy.setTexture(wild_Pokemon->poketex);
		enemy.setPosition(655, -20);
		enemy.setScale(sf::Vector2f(4.5, 4.5));

		Pokemon* our = player->my_team[Our_Pokemon].get();
		me.setOrigin(0, our->poketex2.getSize().y);
		me.setPosition(120, 477);
		me.setScale(sf::Vector2f(4.5, 4.5));
		for (int i = 0; i < player->my_team.size(); i++)
		{
			player->my_team[i]->hp = player->my_team[i]->max_Hp;
		}
		text->all = "your turn";
		text->show = "";
		text->i = 0;
	}
	int waiting = -1;
	virtual void Play(sf::RenderWindow& window) {

		window.clear(sf::Color::Black);
		window.draw(backgr);
		enemy_name.setString(wild_Pokemon->name + "\nLV" + std::to_string(wild_Pokemon->LV));
		my_name.setString(player->my_team[Our_Pokemon].get()->name + "\nLV" + std::to_string(player->my_team[Our_Pokemon].get()->LV));
		window.draw(enemy_name);
		window.draw(my_name);
		window.draw(enemy);
		me.setTexture(player->my_team[Our_Pokemon].get()->poketex2);
		window.draw(me);
		my_Hp->Draw(window, player->my_team[Our_Pokemon].get()->hp, player->my_team[Our_Pokemon].get()->max_Hp);
		enemy_Hp->Draw(window, wild_Pokemon->hp, wild_Pokemon->max_Hp);
		if (attack->Draw(window) && waiting == -1) {
			Attack();
		}
		if (pokeball->Draw(window) == true && waiting == -1) {
			Catch();
		}
		if (pokemon->Draw(window) && waiting == -1) {
			Back = 1;
			Scene = 3;
		}
		if (Run->Draw(window) && waiting == -1 && sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			 Running();
		}

		if (event.text.unicode == 13) { if (waiting == 2) {
			Scene = 4;
			Load();
		} 
		if (waiting == 0) { Scene = 1; Load(); }
		if (waiting == 3) Enemy_Turn();
		if (waiting == 1) { Scene = 0; Load();}
		waiting = -1; }
		text->Show(window);
		window.display();
		
	}
	
	void Attack() {
		if (player->my_team[Our_Pokemon]->atack > wild_Pokemon->def)
			wild_Pokemon->hp -= player->my_team[Our_Pokemon]->atack - wild_Pokemon->def;
		else wild_Pokemon->hp--;
		text->show = "";
		text->i = 0;
		if (wild_Pokemon->hp > 0)
		{
			text->all = { player->my_team[Our_Pokemon]->name + " atack!" };
			waiting = 3;
		}
		else {
			text->all = { player->my_team[Our_Pokemon]->name + " win!" }; waiting = 2; wild_Pokemon->hp = 0; player->my_team[Our_Pokemon]->LevelUp();
		}

	}
	void Enemy_Turn() {
		if (player->my_team[Our_Pokemon]->def < wild_Pokemon->atack)
			player->my_team[Our_Pokemon]->hp -= wild_Pokemon->atack - player->my_team[Our_Pokemon]->def;
		else player->my_team[Our_Pokemon]->hp--;
		text->show = "";
		text->i = 0;
		if(player->my_team[Our_Pokemon]->hp > 0)
		text->all = { wild_Pokemon->name + " atack!" };
		else {
			player->my_team[Our_Pokemon]->hp = 0;
			//text->all = { player->my_team[Our_Pokemon]->name + " fall!" };
			waiting = 5;
			for (int i=0; i< player->my_team.size(); i++)
			{
				if (player->my_team[i]->hp > 0) { Our_Pokemon = i; text->all = { player->my_team[Our_Pokemon]->name + " start!" }; }
			}
			if (player->my_team[Our_Pokemon]->hp == 0) {
				text->all = {  "Game over!" };
				waiting = 1;
			}
		}
	}
	void Catch() {
		srand(time(0));
		if (rand() % 2 == 1)
		{
			std::cout << "Done"; 
			player->Join_the_Team(wild_Pokemon); 
			waiting = 0;
			//Scene = 0;
			text->show = "";
			text->i = 0;
			text->all = { wild_Pokemon->name + " gotcha!" };
			
		}
		else {
			text->show = "";
			text->i = 0;
			text->all = { "Failed" };
			waiting = 3;
		}
	}
	void Running() {
		/*if (waiting == -1) {
			Scene = 0;
			Start();
		}*/
		if (waiting == -1) {
			srand(time(0));
			if (rand() % 2 == 1)
			{
				text->show = "";
				text->i = 0;
				text->all = "Running \n successfull";
				waiting = 2;
			}
			else {
				text->show = "";
				text->i = 0;
				text->all = "Running not \n successfull";
				std::cout << waiting;
				waiting = 3;
			}
		}
	}

	~Battle_Window() {
		delete my_Hp;
		delete enemy_Hp;
		delete attack;
		delete pokemon;
		delete pokeball;
		delete Run;
		delete text;
		std::cout << " battle delele ";

	}
	Pokemon* wild_Pokemon;

protected:
	Hp_Bar* my_Hp;
	Hp_Bar* enemy_Hp;
	sf::Texture back;
	sf::Sprite backgr;
	sf::Sprite me;
	sf::Sprite enemy;
	Button* attack;
	Button* pokemon;
	Button* pokeball;
	Button* Run;

	Textes* text;
	sf::Text enemy_name;
	sf::Text my_name;

	sf::Font arial;
	
};





class Input_Window: public Window {
public:
	
	 Input_Window() {
		std::cout << "load";
		arial.loadFromFile("img/Kenney Future Narrow.ttf");


		text.setFillColor(sf::Color::Black);
		text.setFont(arial);
		text.setString(r);
		text.setPosition(200, 212);
		text.setCharacterSize(50);
		
		if (!inp.loadFromFile("img/input.png")) {
			std::cout << "No png";
		}
		back.setTexture(inp);


		t.setFillColor(sf::Color::Black);
		t.setFont(arial);
		t.setPosition(200, 282);
		t.setString(s);
		t.setCharacterSize(50);
	}
	sf::Texture inp;
	sf::Font arial;
	sf::Text text;
	sf::Text t;
	std::string r = "Enter your pokemon name:";
	std::string s = "";
	std::string o;
	sf::Sprite back;
	

	void Play(sf::RenderWindow& window) {

		t.setString(s);
		window.clear(sf::Color::Black);
		window.draw(back);
		window.draw(text);
		window.draw(t);
		window.display();
	}

	void Text_Enter(sf::RenderWindow& window) {

		
			if (event.type == sf::Event::TextEntered) {
				if (event.text.unicode < 128) {
					if (event.text.unicode == 8) {
						s = s.substr(0, s.size() - 1);
					}
					else if (event.text.unicode == 13)
					{
						if (s != "")
						player->my_team[player->my_team.size() - 1].get()->name = s;
						Scene = 4;
						s = "";
						player->Save();
					}
					else
						s += static_cast<char>(event.text.unicode);
				}
			}
		}
};





class World_Window: public Window {
public:
	sf::Sprite back;
	sf::Texture pole;
	sf::Sprite chara;
	sf::Texture up[3];
	sf::Texture down[3];
	sf::Texture left[3];
	sf::Texture right[3];
	World_Window() {
		/*rect.setSize(sf::Vector2f(960, 360));
		rect.setPosition(80, 240);
		rect.setFillColor(sf::Color::Green);*/

		if (!pole.loadFromFile("img/pole.png")) {
			std::cout << "No png";
		}
		back.setTexture(pole);
		for (int i = 1; i < 4; i++) {
			if (!down[i-1].loadFromFile({ "img/" + std::to_string(i) + ".png" })) {
				std::cout << "No png";
			}
		}
		for (int i = 1; i < 4; i++) {
			if (!up[i-1].loadFromFile({ "img/3" + std::to_string(i) + ".png" })) {
				std::cout << "No png";
			}
		}
		for (int i = 1; i < 4; i++) {
			if (!left[i-1].loadFromFile({ "img/1" + std::to_string(i) + ".png" })) {
				std::cout << "No png";
			}
		}
		for (int i = 1; i < 4; i++) {
			if (!right[i-1].loadFromFile({ "img/2" + std::to_string(i) + ".png" })) {
				std::cout << "No png";
			}
		}
		chara.setTexture(down[0]);
	}
	bool Collision(int X, int Y) {
		if (sf::IntRect(0, 0, 1150, 100).contains(X, Y)) return true;
		if (sf::IntRect(0, 0, 50, 700).contains(X, Y)) return true;
		if (sf::IntRect(1050, 0, 100, 700).contains(X, Y)) return true;
		if (sf::IntRect(0, 600, 1150, 100).contains(X, Y)) return true;
		return false;
	}
	void Play(sf::RenderWindow& window) {
		i++;
		if (i == 240) i = 0;
		window.clear(sf::Color::Black);
		window.draw(back);
		if (event.text.unicode == 115 && (!Collision(x, y+150) || !Collision(x + 150, y + 150))) {
			y++; chara.setTexture(down[i / 80]);
			WildP();
		}
		if (event.text.unicode == 119 && (!Collision(x, y) || !Collision(x + 150, y))) {
			y--; chara.setTexture(up[i / 80]);
			WildP();
		}
		if (event.text.unicode == 97 && (!Collision(x, y) || !Collision(x, y+150))) {
			x--; chara.setTexture(left[i / 80]);
			WildP();
		}
		if (event.text.unicode == 100 && (!Collision(x+150, y) || !Collision(x + 150, y+150))) {
			x++; chara.setTexture(right[i / 80]);
			WildP();
		}
		
		chara.setPosition(x, y);
		window.draw(chara);
		//window.draw(rect);
		window.display();
	}
	void WildP() {
		srand(time(0));
		if (sf::IntRect(80, 240, 960, 360).contains(x, y + 150) && rand() % 10 == 0) {
			Scene = 2;
		}
	}
	int x = 500;
	int y = 255;
	int i = 0;
	sf::RectangleShape rect;
};
