#include <vector>
#include <fstream>
#include <iostream>
#include <SFML\Graphics.hpp>

class Pokemon
{
public:
	int hp;
	int max_Hp;
	int atack;
	int def;
	void LevelUp() {
		max_Hp *= 1.02;
		atack *= 1.04;
		def *= 1.02;
		LV++;
	}
	void SetName(std::string new_name) {
		if (new_name != "no")
			name = new_name;
	}
	Pokemon() {
		name = "Pokemon";
	}
	std::string name;
	sf::Texture poketex;
	sf::Texture poketex2;
	sf::Texture poketex3;
	int LV = 1;
	std::string type;
};

class Eevee : public Pokemon {
public:
	Eevee(int i) {
		name = "Eevee";
		type = name;
		max_Hp = 55;
		atack = 55;
		def = 50;
		for (int k = 1; k < i; k++) {
			LevelUp();
		}
		hp = max_Hp;
		if (!poketex.loadFromFile("img/133.png")) {
			std::cout << "No png";
		}
		if (!poketex2.loadFromFile("img/133 (1).png")) {
			std::cout << "No png";
		}
		if (!poketex3.loadFromFile("img/1331.png")) {
			std::cout << "No png";
		}
	}

};

class Vaporeon : public Pokemon {
public:
	Vaporeon(int i) {
		name = "Vaporeon";
		type = name;
		max_Hp = 130;
		atack = 65;
		def = 60;
		for (int k = 1; k < i; k++) {
			LevelUp();
		}
		hp = max_Hp;
		if (!poketex.loadFromFile("img/134.png")) {
			std::cout << "No png";
		}
		if (!poketex2.loadFromFile("img/134 (1).png")) {
			std::cout << "No png";
		}
		if (!poketex3.loadFromFile("img/1341.png")) {
			std::cout << "No png";
		}
	}
};

class Flareon : public Pokemon {
public:
	Flareon(int i) {
		name = "Flareon";
		type = name;
		max_Hp = 65;
		atack = 130;
		def = 60;
		for (int k = 1; k < i; k++) {
			LevelUp();
		}
		hp = max_Hp;
		if (!poketex.loadFromFile("img/136.png")) {
			std::cout << "No png";
		}
		if (!poketex2.loadFromFile("img/136 (1).png")) {
			std::cout << "No png";
		}
		if (!poketex3.loadFromFile("img/1361.png")) {
			std::cout << "No png";
		}
	}

};

class Jolteon : public Pokemon {
public:
	Jolteon(int i) {
		name = "Jolteon";
		type = name;
		max_Hp = 65;
		atack = 65;
		def = 60;
		for (int k = 1; k < i; k++) {
			LevelUp();
		}
		hp = max_Hp;
		if (!poketex.loadFromFile("img/135.png")) {
			std::cout << "No png";
		}
		if (!poketex2.loadFromFile("img/135 (1).png")) {
			std::cout << "No png";
		}
		if (!poketex3.loadFromFile("img/1351.png")) {
			std::cout << "No png";
		}
	}
};

class Player {
public:
	std::vector<std::shared_ptr <Pokemon>>my_team;
	std::string name;


	void Join_the_Team(Pokemon* newbie) {
		std::shared_ptr<Pokemon> poke(newbie);
		my_team.push_back(poke);

	}
	void Save() {
		std::ofstream save("save_file.txt");
		for (int i = 0; i < my_team.size(); i++)
			save << my_team[i].get()->name << " " << my_team[i].get()->LV << " " << my_team[i].get()->type << std::endl;

	}
	void Load() {
		std::string name;
		std::string type;
		int lv;
		my_team.clear();
		std::ifstream load("save_file.txt");
		while (!load.eof()) {
			load >> name >> lv >> type;
			if (type == "Eevee")
				Join_the_Team(new Eevee(lv));
			if (type == "Vaporeon")
				Join_the_Team(new Vaporeon(lv));
			if (type == "Flareon")
				Join_the_Team(new Flareon(lv));
			if (type == "Jolteon")
				Join_the_Team(new Jolteon(lv));
			my_team[my_team.size() - 1].get()->name = name;
		}
		my_team.pop_back();
	}
};
#pragma once
