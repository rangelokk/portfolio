﻿# Упрощенная игра  Pokemon
## Системные требования
Windows 10 x64

## Для разработки
Необходим Cmake 3.15, компилятор VS 2019


## Об игре
Вы можете ловить покемонов и давать им имена, если все ваши покемоны умрут игра закончится, игра поддерживает автоматическое сохранение. Во время боя вы можете менять покемонов, используйте это чтобы победить более сильных противников. При победе уровень текущего покемона растет, как и уровень ваших противников. По краям сцены расставлены коллайдеры, так что вы не сможете уйти за пределы сцены. Нажимайте кнопки и Enter во время боя.

Пользователь может изменить спрайты, или провести рефакторинг кода.
