﻿#include <SFML\Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <vector>
#include "Object.hpp"
#include "Pokemon.h"
#include <ctime> //для рандома
#include <fstream>
#include <memory>

//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

using namespace std::chrono_literals;
using namespace std;

//vector<shared_ptr <Pokemon>>my_team;


Player player;



int main()
{
	player.Join_the_Team(new Eevee(5));
	vector<shared_ptr <Window>>Wind;
	shared_ptr<Window> start (new Start_Window());
	start->player = &player;
	Wind.push_back(start);
	shared_ptr<Window> input(new Input_Window());
	Wind.push_back(input);
	input->player = &player;
	shared_ptr<Window> battle(new Battle_Window());
	battle->player = &player;
	Wind.push_back(battle);
	shared_ptr<Window> inventory(new Pokemon_Inventory());
	inventory->player = &player;
	Wind.push_back(inventory);
	shared_ptr<Window> world(new World_Window());
	Wind.push_back(world);

	sf::RenderWindow window;
	    window.create(sf::VideoMode(1150, 700), "Pokemon");
		
		
		Wind[2].get()->Load();
		Wind[3].get()->Load();

		while (window.isOpen()) {
			

			while (window.pollEvent(event)) {
				if (event.type == sf::Event::Closed) {
					window.close();
				}
				if (Scene == 1) {
					Wind[Scene].get()->Text_Enter(window);   
				}
				
			}
			
			Wind[Scene].get()->Play(window);    
			
		}
		player.Save();
}

